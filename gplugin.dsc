Format: 3.0 (native)
Source: gplugin
Binary: libgplugin0, libgplugin0-dbg, libgplugin-dev, libgplugin-loaders, gir1.2-gplugin-0.0, libgplugin-python
Architecture: any all
Version: 0.0.19
Maintainer: Gary Kramlich <grim@reaperworld.com>
Homepage: https://bitbucket.org/gplugin/main
Standards-Version: 3.9.6
Vcs-Browser: https://bitbucket.org/gplugin/main/src
Vcs-Hg: https://bitbucket.org/gplugin/main
Build-Depends: debhelper (>= 9), cmake, libglib2.0-dev, xsltproc, gettext, help2man, gobject-introspection, libgirepository1.0-dev, python3-dev, python-gi-dev, python3-gi
Package-List:
 gir1.2-gplugin-0.0 deb introspection optional arch=any
 libgplugin-dev deb libdevel optional arch=any
 libgplugin-loaders deb libs optional arch=all
 libgplugin-python deb libs optional arch=any
 libgplugin0 deb libs optional arch=any
 libgplugin0-dbg deb debug extra arch=any
Checksums-Sha1:
 f935e97200a9492db46ba8b44953ecf99db9d3d9 296864 gplugin_0.0.19.tar.xz
Checksums-Sha256:
 1613a204b659c0fcd62040eb309cd1eb96e93d0ab60d62bee70b426b22751b49 296864 gplugin_0.0.19.tar.xz
Files:
 b58fe4658f8437e4b4eed59a09989aa5 296864 gplugin_0.0.19.tar.xz
