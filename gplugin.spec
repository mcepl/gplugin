%if 0%{?fedora} > 0
%define buildpython 1
%else
%define buildpython 0
%endif
%global owner rw_grim

Name:		gplugin
Version:	0.0.19
Release:	1%{?dist}
License:	LGPL-2.0+
Summary:	A GObject based library that implements a reusable plugin system
Url:		https://bitbucket.org/rw_grim/gplugin/overview
Group:		Development/Libraries
# https://bitbucket.org/%%{owner}/%%{name}/get/v%%{version}.tar.bz2
Source0:	%{name}-%{version}.tar.gz
#Source0:	https://bitbucket.org/%{owner}/%{name}/get/v%{version}.tar.bz2
# https://bitbucket.org/gplugin/main/issue/41
Patch0001:	%{name}-fix-lua-check.diff
# https://bitbucket.org/gplugin/main/issue/65
Patch0002:	%{name}-library-suffix.diff
Patch0003:	%{name}-fix-cmake2.patch

BuildRequires:	cmake >= 2.8
BuildRequires:	glib2-devel >= 2.20.0
BuildRequires:	gobject-introspection-devel
BuildRequires:	gettext
BuildRequires:	gtk3-devel
BuildRequires:	help2man
BuildRequires:	libxslt
%if 0
# Lua build is failing...
BuildRequires:	lua-devel
BuildRequires:	lua-lgi
BuildRequires:	lua-moonscript
%endif
%if %{buildpython}
# there is no pygobject3 for opensuse
BuildRequires:	python3-devel
BuildRequires:	python3-gobject
BuildRequires:	pygobject3-devel
%define nopython 0
%else
%define nopython -DBUILD_PYTHON=FALSE
%endif

%if 0%{?suse_version} > 0
# avoid "directories not owned by a package" error
%define notownedhack 1
%else
%define notownedhack 0
%endif


%package devel
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%package gtk3
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%package gtk3-devel
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}-gtk3%{?_isa} = %{version}-%{release}
Requires:	%{name}-devel%{?_isa} = %{version}-%{release}

#%package lua
#Summary:	A GObject based library that implements a reusable plugin system
#Group:		Development/Libraries
#Requires:	%{name}%{?_isa} = %{version}-%{release}

%package python3
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}


%description
GPlugin is a GObject based library that implements a reusable plugin system
that supports loading plugins in other languages via loaders.  GPlugin also
implements dependencies among the plugins.

It was started due to the infamous "Plugin Problem" for Guifications 3, which
was that I needed plugins that could depend on each other, be able to be
written in other languages, have plugins that are loaded before the main load
phase, and allow plugins to register types into the GObject type system.

%description devel
Development files for %{name} library.

%description gtk3
GTK3 files for %{name} library.

%description gtk3-devel
GTK3 Development files for %{name} library.

#%description lua
#Lua loaders for %{name} library.

%description python3
Python 3 loader for %{name} library.


%prep
%setup -q

%patch1 -p1
%patch2 -p1
%patch3


%build
mkdir -p build
pushd build

CFLAGS="%{optflags}" cmake \
	-Wno-dev \
	-DCMAKE_INSTALL_PREFIX=%{_prefix} \
	-DBUILD_GJS=FALSE \
	-DBUILD_SEED=FALSE \
	-DBUILD_LUA=FALSE \
	%{nopython} \
	-DLIBDIR=%{_lib} \
	..

make %{?_smp_mflags}


%install
pushd build

make install DESTDIR=%{buildroot}

# Unneeded files
rm -rf %{buildroot}%{_datadir}/doc/gplugin/
%if %{notownedhack} == 1
rm -rf %{buildroot}%{_datadir}/gplugin/
%endif

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root)
%doc README ChangeLog
%if %{notownedhack} == 0
%license COPYING
%endif
%{_bindir}/gplugin-query
%{_libdir}/libgplugin.so.0.1.0
%{_libdir}/gplugin/
%{_libdir}/girepository-1.0/GPlugin-0.0.typelib
%{_datadir}/gir-1.0/GPlugin-0.0.gir
%{_mandir}/man1/gplugin-query.1*

%files devel
%defattr(-,root,root)
%doc README HACKING
%if %{notownedhack} == 0
%license COPYING
%endif
%{_includedir}/gplugin-1.0/
%{_libdir}/libgplugin.so
%{_libdir}/libgplugin.so.0
%{_libdir}/pkgconfig/gplugin.pc
%{_libdir}/pkgconfig/gplugin-gtk.pc

%files gtk3
%defattr(-,root,root)
%doc README
%if %{notownedhack} == 0
%license COPYING
%endif
%{_bindir}/gplugin-gtk-viewer
%{_libdir}/libgplugin-gtk.so.0.1.0
%if %{notownedhack} == 0
%{_datadir}/gplugin/gplugin-gtk/
%endif
%{_mandir}/man1/gplugin-gtk-viewer.1*

%files gtk3-devel
%defattr(-,root,root)
%doc README
%if %{notownedhack} == 0
%license COPYING
%endif
%{_libdir}/libgplugin-gtk.so
%{_libdir}/libgplugin-gtk.so.0

%if 0
%files lua
%doc README
%license COPYING
%{_libdir}/gplugin/gplugin-lua.so
%endif

%if %{buildpython}
%files python3
%defattr(-,root,root)
%doc README
%if %{notownedhack} == 0
%license COPYING
%endif
%{_libdir}/gplugin/gplugin-python.so
%endif


%changelog
* Mon May 4 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.18-1
- Initial package release
