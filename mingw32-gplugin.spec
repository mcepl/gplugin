#
# spec file for package mingw32-gplugin
#
# Copyright (c) 2015 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           mingw32-gplugin
Version:        0.0.19
Release:        1%{?dist}
Summary:        A GObject based library that implements a reusable plugin system
License:        LGPL-2.0+
Group:          Development/Libraries
Url:            https://bitbucket.org/rw_grim/gplugin/overview
# https://bitbucket.org/%%{owner}/%%{name}/get/v%%{version}.tar.bz2
Source0:        gplugin-%{version}.tar.gz
Patch0001:      gplugin-fix-lua-check.diff
Patch0002:      gplugin-library-suffix.diff
Patch0003:      gplugin-fix-cmake2.patch
#!BuildIgnore: post-build-checks
BuildRequires:  cmake >= 2.8
BuildRequires:  gobject-introspection-devel
BuildRequires:  gettext
BuildRequires:  help2man
BuildRequires:  libxslt
%if 0%{?fedora} > 0
BuildRequires:  mingw32-binutils
BuildRequires:  mingw32-gcc
BuildRequires:  mingw32-pkg-config
BuildRequires:  mingw32-winpthreads
%else
BuildRequires:  mingw32-cross-binutils
BuildRequires:  mingw32-cross-gcc
BuildRequires:  mingw32-cross-pkg-config
BuildRequires:  mingw32-glib2-devel >= 2.20.0
%endif
BuildRequires:  mingw32-glib2 >= 2.20.0
#BuildRequires:  mingw32-gtk3-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%if 0%{?fedora} == 0
%_mingw32_package_header_debug
%endif
BuildArch:      noarch

%description
GPlugin is a GObject based library that implements a reusable plugin system
that supports loading plugins in other languages via loaders.  GPlugin also
implements dependencies among the plugins.

It was started due to the infamous "Plugin Problem" for Guifications 3, which
was that I needed plugins that could depend on each other, be able to be
written in other languages, have plugins that are loaded before the main load
phase, and allow plugins to register types into the GObject type system.

%package devel
Summary:        A GObject based library that implements a reusable plugin system (development files)
Group:          Development/Libraries/C and C++

%description devel
GPlugin is a GObject based library that implements a reusable plugin system
that supports loading plugins in other languages via loaders.  GPlugin also
implements dependencies among the plugins.

This package contains all necessary include files and libraries needed
to develop applications that require these.

%lang_package

%if 0%{?fedora} == 0
%_mingw32_debug_package
%endif

%prep
%setup -q -n gplugin-%{version}
%patch1 -p1
%patch2 -p1
%patch3

%build
mkdir -p build
pushd build

echo "lt_cv_deplibs_check_method='pass_all'" >> %{_mingw32_cache}
PATH="%{_mingw32_bindir}:$PATH"; export PATH; \
CFLAGS="%{optflags}" %{_mingw32_cmake} \
	-Wno-dev \
	-DCMAKE_INSTALL_PREFIX=%{_mingw32_prefix} \
	-DCMAKE_INSTALL_LIBDIR=%{_mingw32_libdir} \
	-DBUILD_GJS=FALSE \
	-DBUILD_SEED=FALSE \
	-DBUILD_LUA=FALSE \
	-DBUILD_PYTHON=FALSE \
	-DBUILD_GTK3=FALSE \
	-DBUILD_GIR=FALSE \
	-DTESTING_ENABLED=FALSE \
	-DLIBDIR=%{_mingw32_libdir} \
	..

%{_mingw32_make} %{?_smp_mflags} || %{_mingw32_make}


%install
pushd build

%{_mingw32_make} DESTDIR=%{buildroot} install

# Unneeded files
rm -rf %{buildroot}%{_mingw32_datadir}/doc/gplugin/

%files
%defattr(-,root,root)
%doc README ChangeLog
%{_mingw32_libdir}/libgplugin.dll
%{_mingw32_libdir}/gplugin/gplugin-license-check.dll

%files devel
%defattr(-,root,root)
%doc HACKING
%{_mingw32_bindir}/gplugin-query.exe
%{_mingw32_mandir}/man1/gplugin-query.1*
%{_mingw32_includedir}/gplugin-1.0
%{_mingw32_libdir}/libgplugin.dll.a
%{_mingw32_libdir}/pkgconfig/gplugin.pc

%changelog
