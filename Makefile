PKG=gplugin
INSTALL_DIR=$(HOME)/archiv/website/fedorapeople/rpms

all: x86_64/*.rpm

$(PKG)-*.src.rpm: $(PKG).spec *.tar.gz
	rpmbuild --define '_sourcedir $(shell pwd)'\
		--define '_specdir $(shell pwd)' \
		--define '_builddir $(shell pwd)' \
		--define '_srcrpmdir $(shell pwd)' \
		--define '_rpmdir $(shell pwd)' \
		--define 'dist .el7' \
		--define 'rhel 7' \
		--define 'el7 1' \
		-bs $(shell pwd)/$(PKG).spec \
			|& tee rpmbuild-log.txt

x86_64/*.rpm: $(PKG)-*.src.rpm
	rpmbuild --define '_sourcedir $(shell pwd)'\
		--define '_specdir $(shell pwd)' \
		--define '_builddir $(shell pwd)' \
		--define '_srcrpmdir $(shell pwd)' \
		--define '_rpmdir $(shell pwd)' \
		--define 'dist .el7' \
		--define 'rhel 7' \
		--define 'el7 1' \
		-ba $(shell pwd)/$(PKG).spec \
			|& tee rpmbuild-log.txt

brew: brew7

brew7: $(PKG)-*.src.rpm
	brew build --scratch rhel-7.0-build $<

brew6: $(PKG)-*.src.rpm
	brew build --scratch  RHEL-6.5-test $<

koji7: $(PKG)-*.src.rpm
	koji build --scratch epel7 $<

koji: $(PKG)-*.src.rpm
	koji build --scratch rawhide $<

lint: $(PKG)-*.src.rpm x86_64/*.rpm
	rpmlint -i *.src.rpm x86_64/*.rpm

install: $(PKG)-*.src.rpm x86_64/*.rpm
	repomanage --old --nocheck . |xargs rm -fv
	rm -fv $(INSTALL_DIR)/$(PKG)-*.src.rpm
	ln -sf $(shell readlink -f $(PKG)-*.src.rpm) $(INSTALL_DIR)/
	websync

removeold:
	repomanage --old --nocheck . |xargs rm -fv
	repomanage --old --nocheck x86_64/ |xargs rm -fv

clean:
	rm -rf *~ rpmbuild-log.txt
	rm -rf x86_64 *.src.rpm $(PKG)
